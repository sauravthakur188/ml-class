import math

class ActivationLayer():
    def sigmoid(self,x):
        return 1/(1+math.exp(-x))

    def relu(self,x):
        return math.abs(x)